'use strict'

const jwtService = require('@services/token')
const middleware = {}

middleware.authToken = async function (req, res, next) {
  try {
    const payLoad = await verifyTokenSesion(req.headers.authorization)
    if (!payLoad) {
      return res.status(404).send({error: true, tokenExpired: true, msg: 'Permiso denegado' })
    }

    if (payLoad.error === true) {
      return res.status(404).send(payLoad)
    }

    return next()
  } catch (error) {
    return res.status(401).send({error: true, msg: error.message})
  }
}

async function verifyTokenSesion(token) {
  if (!token) return undefined

  const parts = token.split(' ')

  if (parts.length === 2) {
    const scheme = parts[0]
    const token = parts[1]

    if (/^Bearer$/i.test(scheme)) return await jwtService.verifyJWT(token)
    else return { error: true, msg: 'Invalid token' }
  } else return undefined
}

module.exports = middleware
