'use strict'

const express = require('express')
const asyncify = require('express-asyncify')
const api = asyncify(express.Router())
const { getRoles } = require('@controllers/roles')
const { authToken } = require('@middleware/auth')
const URL = '/roles'

api.get(`${URL}`, authToken, getRoles)

module.exports = api
