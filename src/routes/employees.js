'use strict'

const express = require('express')
const asyncify = require('express-asyncify')
const api = asyncify(express.Router())
const { authToken } = require('../middleware/auth')
const { createEmployee, getEmployees, getByNoEmployee, updateEmployee } = require('@controllers/employees')
const URL = '/employee'

api.post(`${URL}`, authToken, createEmployee)
api.get(`${URL}/:id`, authToken, getEmployees)
api.get(`${URL}`, authToken, getByNoEmployee)
api.put(`${URL}/:id`, authToken, updateEmployee)

module.exports = api
