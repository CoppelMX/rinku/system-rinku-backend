'use strict'

const express = require('express')
const asyncify = require('express-asyncify')
const api = asyncify(express.Router())
const { createUser } = require('@controllers/users')
const URL = '/user'

api.post(`${URL}`, createUser)

module.exports = api