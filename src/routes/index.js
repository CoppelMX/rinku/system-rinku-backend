const login = require('@routes/login')
const employee = require('@routes/employees')
const user = require('@routes/users')
const roles = require('@routes/roles')
const movements = require('@routes/movements')

module.exports = [
  login,
  employee,
  user,
  roles,
  movements
]