'use strict'

const express = require('express')
const asyncify = require('express-asyncify')
const api = asyncify(express.Router())
const { login } = require('@controllers/login')

const URL = '/auth'

api.post(`${URL}/login`, login)

module.exports = api