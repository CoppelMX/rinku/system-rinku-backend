'use strict'

const express = require('express')
const asyncify = require('express-asyncify')
const api = asyncify(express.Router())
const { authToken } = require('@middleware/auth')
const { createMovement, getMovements } = require('@controllers/movements')

const URL = '/movements'

api.post(`${URL}`, authToken, createMovement)
api.get(`${URL}`, authToken, getMovements)

module.exports = api
