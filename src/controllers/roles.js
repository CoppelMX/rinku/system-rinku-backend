'use strict'

const serviceRoles = require('@services/roles')
const controller = {}

controller.getRoles = async function (req, res) {
  try {
    const respuesta = await serviceRoles.getRoles()

    return res.send(respuesta)
  } catch (error) {
    return res.status(500).send({ msg: error.message, error: true, details: error })
  }
}

module.exports = controller
