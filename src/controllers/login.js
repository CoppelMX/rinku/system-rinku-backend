'use strict'

const serviceLogin = require('@services/login')
const controller = {}

controller.login = async function (req, res) {
  try {
    const { user, password } = req.body
    const respuesta = await serviceLogin.login(user, password)

    if (respuesta.error) return res.status(400).send(respuesta)

    return res.send(respuesta)
  } catch (error) {
    return res.status(500).send({ error: true, detail: error, msg: error.message,  })
  }
}

module.exports = controller
