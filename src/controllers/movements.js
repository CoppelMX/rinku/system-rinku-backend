'use strict'

const serviceMovimientos = require('@services/movements')
const controller = {}

controller.createMovement = async function (req, res) {
  try {
    const { month, year, employeeId, deliveries } = req.body
    const respuesta = await serviceMovimientos.createMovement(month, year, employeeId, deliveries)

    if (respuesta.error) return res.status(404).send(respuesta)

    return res.send(respuesta)
  } catch (error) {
    return res.status(500).send({ msg: error.message, error: true, details: error })
  }
}

controller.getMovements = async function (req, res) {
  try {
    const { month, year } = req.query
    const respuesta = await serviceMovimientos.getMovements(month, year)

    if (respuesta.error) return res.status(404).send(respuesta)

    return res.send(respuesta)
  } catch (error) {
    return res.status(500).send({ msg: error.message, error: true, details: error })
  }
}

module.exports = controller
