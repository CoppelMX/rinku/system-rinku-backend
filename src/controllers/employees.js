'use strict'

const serviceEmpleado = require('@services/employees')
const controller = {}

controller.createEmployee = async function (req, res) {
  try {
    const { noEmployee, name, rolId } = req.body
    const respuesta = await serviceEmpleado.createEmployee(noEmployee, name, rolId)

    if (respuesta.error === true) return res.status(400).send(respuesta)

    return res.send(respuesta)
  } catch (error) {
    return res.status(500).send({ msg: error.message, error: true, details: error })
  }
}

controller.getEmployees = async function (req, res) {
  try {
    const { id } = req.params
    const { search } = req.query
    const respuesta = await serviceEmpleado.getEmployees(search, +id)

    return res.send(respuesta)
  } catch (error) {
    return res.status(500).send({ msg: error.message, error: true, details: error })
  }
}

controller.getByNoEmployee = async function (req, res) {
  try {
    const { noEmployee } = req.query
    const respuesta = await serviceEmpleado.getByNoEmployee(noEmployee)

    if (respuesta.error === true) return res.status(400).send(respuesta)

    return res.send(respuesta)
  } catch (error) {
    return res.status(500).send({ msg: error.message, error: true, details: error })
  }
}

controller.updateEmployee = async function (req, res) {
  try {
    const { id } = req.params
    const datos = req.body
    const respuesta = await serviceEmpleado.updateEmployee(id, datos.datos)

    if (respuesta.error === true) return res.status(400).send(respuesta)

    return res.send(respuesta)
  } catch (error) {
    return res.status(500).send({ msg: error.message, error: true, details: error })
  }
}

module.exports = controller
