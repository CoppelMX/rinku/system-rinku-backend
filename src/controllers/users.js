'use strict'

const serviceUser = require('@services/users')
const controller = {}

controller.createUser = async function (req, res) {
  try {
    const { name, email, password, rolId } = req.body
    const respuesta = await serviceUser.createUser(name, email, password, rolId)

    if (respuesta.error === true) return res.status(400).send(respuesta)

    return res.send(respuesta)
  } catch (error) {
    return res.status(500).send({ msg: error.message, error: true, details: error })
  }
}

module.exports = controller