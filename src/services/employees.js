'use strict'

const { Employee } = require('../../db/src/database/librerias')
const service = {}

/**
 * Servicio para registrar un empleado
 * @param { Integer } noEmployee - numero de empleado capturado por el cliente
 * @param { String } name - nombre del empleado capturado por el cliente
 * @param { Integer } rolId - id del rol selccionado por el cliente
 */
service.createEmployee = async function (noEmployee, name, rolId) {
  if (!noEmployee || !name || !rolId) return { error: true, msg: 'Parametros incompletos' }

  const EMPLOYEE = await Employee.getByNoEmployee(noEmployee)

  if (EMPLOYEE) return { error: true, msg: 'El empleado ya existe' }

  await Employee.create({
    noEmployee,
    name,
    rolId,
  })

  return { error: false, msg: 'El empleado fue registrado exitosamente' }
}

/**
 * Servicio para obtener el listado de empleados ya sea por busqueda o la lista completa
 * @param { String } search - Campo no obligatorio en caso de llegar se va a filtrar la busqueda por nombre o numero de empleado
 * @param { Integer } id - Campo no obligatorio en caso de llegar se va a filtrar la busqueda el id del empleo
 */
service.getEmployees = async function (search, id) {
  let data

  if (!isNaN(id)) {
    data = await Employee.getById(id)
  } else {
    data = await Employee.get(search)
  }

  return { error: false, data }
}

/**
 * Servicio para obtener los datos de un empleado por filtrado por numero de empleado
 * @param { Integer } noEmployee - numero de empleado enviado por el cliente
 */
service.getByNoEmployee = async function (noEmployee) {
  if (!noEmployee) return { error: true, msg: 'Parametros incompletos' }

  const EMPLOYEE = await Employee.getByNoEmployee(noEmployee)

  if (!EMPLOYEE) return { error: true, msg: 'El emploeado no existe' }

  return EMPLOYEE
}

/**
 * Servicio para actualizar un empleado
 * @param { Integer } id - id de empleado enviado por el cliente
 * @param { Object } datos - datos del empleado enviado por el cliente para actualizar
 */
service.updateEmployee = async function (id, datos) {
  if (!id) return { error: true, msg: 'Parametros incompletos' }

  const EMPLOYEE = await Employee.getById(id)

  if (!EMPLOYEE) return { error: true, msg: 'El empleado no existe' }

  const resultado = await Employee.update(id, datos)

  return { error: false, resultado }
}

// service.getById = async function (id) {
//   if (!id) return { error: true, msg: 'Parametros incompletos' }

//   const resultado = await Employee.getById(id)

//   return resultado
// }

module.exports = service
