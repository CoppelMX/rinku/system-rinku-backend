'use strict'

const { User } = require('../../db/src/database/librerias')
const passwordService = require('./encrypt')
const service = {}

/**
 * Servicio dar de alta usuarios al sistema
 * @param { String } name - nombre del usuario a registrar
 * @param { String } email - correo del usuario a registrar
 * @param { String } password - contraseña del usuario a registrar
 * @param { Integer } rolId - id del rol al que pertenecera el usuario a registrar
 */
service.createUser = async function (name, email, password, rolId) {
  if (!name || !email || !password || !rolId) return { error: true, msg: 'Parametros incompletos' }

  const USER = await User.getByEmail(email)

  if (USER) return { error: true, msg: 'El usuario ya existe' }

  const passwordEncrypt = await passwordService.encrypterPass(password)
  await User.create({
    name,
    email,
    password: passwordEncrypt,
    rolId,
  })
  
  return { error: false, msg: 'Se ha creado exitosamente el usuario' }
}

module.exports = service