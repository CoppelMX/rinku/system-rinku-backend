'use strict'

const { User } = require('../../db/src/database/librerias')
const passwordService = require('./encrypt')
const tokenJWTService = require('./token')
const service = {}

/**
 * Servicio para iniciar sesion y devolver datos de usuario para guardar en el store del front
 * @param { String } user - correo electronico del usuario
 * @param { String } password - contraseña del usuario
 */
service.login = async function (user, password) {
  if (!user || !password) return { error: true, msg: 'Parametros incompletos' }

  const USER = await User.getByEmail(user)

  if (!USER) return { error: true, msg: 'Usuario y/o contraseña incorrectos' }

  const decryptPassword = await passwordService.descrypterPass(USER.password)
  
  if (decryptPassword === password) {
    const tokenJWT = await tokenJWTService.generateJWT(USER)
    const data = {
      nombre: USER.name,
      rol: USER.description,
      rolId: USER.rolid,
      token: tokenJWT.token
    }

    return { error: false, data }
  } else return { error: true, msg: 'Usuario y/o contraseña incorrectos' }
}

module.exports = service
