'use strict'
const JWT = require('jsonwebtoken')
const { TOKEN_JWT_KEY, TOKEN_EXPIRES_IN } = process.env
const TOKEN_EXPIRES = parseInt(TOKEN_EXPIRES_IN, 10)
const service = {}

service.generateJWT = async function (user) {
  const token = JWT.sign(user, TOKEN_JWT_KEY, {
    algorithm: 'HS256',
    expiresIn: TOKEN_EXPIRES * 60,
  })

  return { error: false, token }
}

service.verifyJWT = async function (token) {
  const tokenVerified = JWT.verify(token, TOKEN_JWT_KEY, { algorithms: ['HS256'] }, (error, decoded) => {
      if (error) {
        return {
          error: true,
          tokenExpired: true,
          msg: 'Token no valido',
        }
      }
      return {
        error: false,
        tokenVerified: decoded,
      }
    }
  )
  return tokenVerified
}

module.exports = service
