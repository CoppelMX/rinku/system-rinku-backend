'use strict'

const { Movement, Employee } = require('../../db/src/database/librerias')
const ISR_PERCENTAGE_RETENTION = 9 / 100
const PERCENTAGE_PANTRY_VOUCHERS = 4 / 100
const BASE_SALARY = 30
const EXTRA_DELIVERY_AMOUNT = 5
const MAX_SALARY_WITHOUT_EXTRA_RETENTION = 10000
const ISR_EXTRA_RETENTION = 3 / 100
const BONUS_PER_HOUR = [
  {
    rolId: 2,
    amount: 10,
  },
  {
    rolId: 3,
    amount: 5,
  },
  {
    rolId: 4,
    amount: 0,
  },
]
const service = {}

/**
 * Servicio para registrar un movimiento en el sistema
 * @param { Integer } month - mes de registro de movimiento enviado por el cliente
 * @param { String } year - año de registro de movimiento enviado por el cliente el formato es 'YYYY'
 * @param { Integer } employeeId - id del empleado
 * @param { Integer } deliveries - cantidad de entregas realizadas por el empleado
 */
service.createMovement = async function (month, year, employeeId, deliveries) {
  if (!month || !year || !employeeId || !deliveries) return { error: true, msg: 'Parametros incompletos' }

  const EMPLOYEE = await Employee.getById(employeeId)

  if (!EMPLOYEE) return { error: true, msg: 'No se encontro el empleado' }

  const { salary, hours, paymentDeliveries, bonus, retentionISR, pantryVouchers, extraRetentionISR } = await calculateAmount(EMPLOYEE.rolid, deliveries)

  await Movement.create({
    employeeId: EMPLOYEE.noemployee,
    month,
    year,
    salary,
    hours,
    deliveries,
    paymentDeliveries,
    bonus,
    retentionISR,
    pantryVouchers,
    extraRetentionISR
  })

  return { error: false, msg: 'Movimiento generado exitosamente' }
}

/**
 * Servicio para obtener los movimientos por año y mes
 * @param { Integer } month - mes de registro que se requiere visualizar
 * @param { String } year - año de registro que se requiere visualizar el formato es 'YYYY'
 */
service.getMovements = async function (month, year) {
  if (!month || !year) return { error: true, msg: 'Parametros incompletos' }

  const resultado = await Movement.get(month, year)

  return resultado
}

/**
 * Funcion para desglozar los montos que se requieren para guardar un movimiento
 * @param { Integer } rolId - id del rol del empleado
 * @param { Integer } deliveries - numero de entregas realizadas por el empleado
 */
async function calculateAmount(rolId, deliveries) {
  const hoursPerMonth = 8 * 6 * 4
  const baseSalary = BASE_SALARY * hoursPerMonth

  const paymentDeliveries = EXTRA_DELIVERY_AMOUNT * deliveries

  const bonusPerHour = BONUS_PER_HOUR.find((item) => item.rolId == rolId)
  const extraBonusPerHour = bonusPerHour.amount * hoursPerMonth
  
  const totalGrossSalary = baseSalary + paymentDeliveries + extraBonusPerHour
  const retentionISR = totalGrossSalary * ISR_PERCENTAGE_RETENTION
  const totalNetSalary = totalGrossSalary - retentionISR

  let salary = 0
  let extraRetentionISR = 0

  if (totalNetSalary > MAX_SALARY_WITHOUT_EXTRA_RETENTION) {
    extraRetentionISR = totalNetSalary * ISR_EXTRA_RETENTION
    salary = totalNetSalary - extraRetentionISR
  } else salary = totalNetSalary

  const pantryVouchers = salary * PERCENTAGE_PANTRY_VOUCHERS

  return { salary, hours: hoursPerMonth, paymentDeliveries, bonus: extraBonusPerHour, retentionISR, pantryVouchers, extraRetentionISR }
}

module.exports = service
