'use strict'

const { Rol } = require('../../db/src/database/librerias')
const service = {}

/**
 * Servicio para obtener los roles de la bd excluyendo el rol administrador
 */
service.getRoles = async function () {
  const resultado = await Rol.get()

  return resultado
}

module.exports = service
