'use strict'

const Crypto = require('node-cryptojs-aes')
const CryptoJS = Crypto.CryptoJS
const { CRYPTO_PASSWORD } = process.env
const service = {}

service.generatePassword = function (password = 123) {
  const encryptPassword = service.encrypterPass(password)

  return encryptPassword
}

service.encrypterPass = function (password) {
  return CryptoJS.AES.encrypt(password, CRYPTO_PASSWORD).toString()
}

service.descrypterPass = function (password) {
  return CryptoJS.AES.decrypt(password, CRYPTO_PASSWORD).toString(CryptoJS.enc.Utf8)
}

module.exports = service