-- Database: rinku

-- DROP DATABASE IF EXISTS rinku;

CREATE DATABASE rinku;

CREATE TABLE roles (
    id SERIAL PRIMARY KEY,
    description VARCHAR(100) NOT NULL,
    status SMALLINT NOT NULL DEFAULT '1',
    createdAt TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updatedAt TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
);

INSERT INTO roles (description, status) VALUES ('Administrador', 1);
INSERT INTO roles (description, status) VALUES ('Chofer', 1);
INSERT INTO roles (description, status) VALUES ('Cargador', 1);
INSERT INTO roles (description, status) VALUES ('Auxiliar', 1);

CREATE TABLE users (
    id SERIAL PRIMARY KEY,
	name VARCHAR(250) NOT NULL,
    email VARCHAR(50) DEFAULT NULL,
    password VARCHAR(100) DEFAULT NULL,
    rolId INT NOT NULL,
    status SMALLINT NOT NULL DEFAULT '1',
    createdAt TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updatedAt TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
);

ALTER TABLE users ADD CONSTRAINT fk_rolId FOREIGN KEY (rolId) REFERENCES roles (id);

CREATE TABLE employees (
    id SERIAL PRIMARY KEY,
    noEmployee INT NOT NULL,
    name VARCHAR(250) NOT NULL,
    rolId INT NOT NULL,
    status SMALLINT NOT NULL DEFAULT '1',
    createdAt TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updatedAt TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
);

ALTER TABLE employees ADD CONSTRAINT fk_rolId FOREIGN KEY (rolId) REFERENCES roles (id);

CREATE TABLE movements (
    id SERIAL PRIMARY KEY,
    employeeId INT NOT NULL,
    month CHAR NOT NULL,
    year INT NOT NULL,
    salary DECIMAL(13,4) NOT NULL,
    status SMALLINT NOT NULL DEFAULT '1',
    createdAt TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updatedAt TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
);

ALTER TABLE movements ADD CONSTRAINT fk_employeeId FOREIGN KEY (employeeId) REFERENCES employees (id);

--------------------------------------------------------------------------------------------------------
--======================================================================================================
--------------------------------------------------------------------------------------------------------

CREATE OR REPLACE FUNCTION getUserByEmail(emailUser VARCHAR(50)) 
RETURNS TABLE (name VARCHAR, password VARCHAR, rolId INTEGER, description VARCHAR)
LANGUAGE 'plpgsql'
AS $BODY$ 
BEGIN
	RETURN QUERY SELECT users.name, users.password, users.rolId, rol.description
	FROM users AS users
	INNER JOIN roles AS rol ON users.rolId = rol.id
	WHERE users.status = 1 and users.email = emailUser LIMIT 1; 
END;
$BODY$;

select * From getUserByEmail ('juancarlosg9004@gmail.com');


CREATE OR REPLACE FUNCTION getRoles() 
RETURNS TABLE (id INTEGER, description VARCHAR)
LANGUAGE 'plpgsql'
AS $BODY$ 
BEGIN
	RETURN QUERY SELECT roles.id, roles.description
	FROM roles as roles
	WHERE roles.status = 1 and roles.id != 1; 
END;
$BODY$;

select * From getRoles ();

CREATE OR REPLACE FUNCTION getByNoEmployee(nomberEmployee INTEGER) 
RETURNS TABLE (id INTEGER, noEmployee INTEGER, name VARCHAR, rolId INTEGER, description VARCHAR)
LANGUAGE 'plpgsql'
AS $BODY$ 
BEGIN
	RETURN QUERY SELECT employees.id, employees.noEmployee, employees.name, rol.id AS rolId, rol.description
	FROM employees AS employees
	INNER JOIN roles AS rol ON employees.rolId = rol.id
	WHERE employees.noEmployee = nomberEmployee LIMIT 1; 
END;
$BODY$;

select * From getByNoEmployee (1);

CREATE OR REPLACE PROCEDURE createEmployee(noEmployee INTEGER, name VARCHAR(250), rolId integer)
LANGUAGE 'plpgsql'
AS $BODY$
BEGIN
    INSERT INTO employees(noEmployee, name, rolId, status)
    VALUES(noEmployee, name, rolId, 1); 
END
$BODY$;

CALL createEmployee(1, 'Juan Carlos Guerrero Ortiz', 2);
select * from employees;
truncate table movement_detail, movements, employees;

CREATE OR REPLACE FUNCTION getByEmployeeById(_id INTEGER) 
RETURNS TABLE (id INTEGER, noEmployee INTEGER, name VARCHAR, rolId INTEGER, status SMALLINT, createdAt TIMESTAMP, updatedAt TIMESTAMP)
LANGUAGE 'plpgsql'
AS $BODY$ 
BEGIN
	RETURN QUERY SELECT employees.id, employees.noEmployee, employees.name, employees.rolId, employees.status, employees.createdAt, employees.updatedAt
	FROM employees AS employees
	WHERE employees.id = _id LIMIT 1; 
END;
$BODY$;

select * From getByEmployeeById (1);

CREATE OR REPLACE FUNCTION getEmployeeWithFilters(_search VARCHAR(250)) 
RETURNS TABLE (id INTEGER, noEmployee INTEGER, name VARCHAR, rolId INTEGER, description VARCHAR)
LANGUAGE 'plpgsql'
AS $BODY$ 
BEGIN
	RETURN QUERY SELECT employees.id, employees.noEmployee, employees.name, rol.id AS rolId, rol.description
	FROM employees AS employees
	INNER JOIN roles AS rol ON employees.rolId = rol.id
	WHERE employees.name LIKE '%' || _search || '%' OR employees.noEmployee::VARCHAR LIKE '%' || _search || '%'; 
END;
$BODY$;

select * From getEmployeeWithFilters ('juan carlos');

CREATE OR REPLACE FUNCTION getEmployees() 
RETURNS TABLE (id INTEGER, noEmployee INTEGER, name VARCHAR, rolId INTEGER, description VARCHAR)
LANGUAGE 'plpgsql'
AS $BODY$ 
BEGIN
	RETURN QUERY SELECT employees.id, employees.noEmployee, employees.name, rol.id AS rolId, rol.description
	FROM employees AS employees
	INNER JOIN roles AS rol ON employees.rolId = rol.id; 
END;
$BODY$;

select * From getEmployees ();

CREATE OR REPLACE PROCEDURE updateEmployeeById(_id INTEGER, _noEmployee INTEGER, _name VARCHAR(250), _rolId INTEGER)
LANGUAGE 'plpgsql'
AS $BODY$
BEGIN
    UPDATE employees
	SET
		noEmployee = _noEmployee,
		name = _name,
		rolId = _rolId
	WHERE id = _id;
END
$BODY$;

CALL updateEmployeeById(2, 2, 'test', 3);
select * from employees;
truncate table movement_detail, movements, employees;

CREATE OR REPLACE PROCEDURE createMovement(employeeId INTEGER, month CHAR, year INTEGER, salary DECIMAL(13, 4), hours INTEGER, deliveries INTEGER, paymentDeliveries DECIMAL(13, 4), bonus DECIMAL(13, 4), retentionISR DECIMAL(13, 4), pantryVouchers DECIMAL(13, 4), extraRetentionISR DECIMAL(13, 4))
LANGUAGE 'plpgsql'
AS $BODY$
DECLARE
   movementId INTEGER;
BEGIN
    INSERT INTO movements (employeeId, month, year, salary)
    VALUES(employeeId, month, year, salary)
	RETURNING id INTO movementId;
	
	INSERT INTO movement_detail (movementId, hours, deliveries, paymentDeliveries, bonus, retentionISR, pantryVouchers, extraRetentionISR)
	VALUES (movementId, hours, deliveries, paymentDeliveries, bonus, retentionISR, pantryVouchers, extraRetentionISR);
END
$BODY$;

CALL createMovement(250, '5', 2023, 30000, 80, 10, 500, 200, 3000, 1000, 150);
select * from movements;
select * from movement_detail;
truncate table movement_detail, movements, employees;

CREATE OR REPLACE FUNCTION getMovementsByDate(_month CHAR, _year INTEGER) 
RETURNS TABLE (id INTEGER, salary DECIMAL, hours INTEGER, deliveries INTEGER, paymentDeliveries DECIMAL, bonus DECIMAL, retentionISR DECIMAL,  pantryVouchers DECIMAL, extraRetentionISR DECIMAL, noEmployee INTEGER, name VARCHAR, rolId INTEGER, description VARCHAR)
LANGUAGE 'plpgsql'
AS $BODY$ 
BEGIN
	RETURN QUERY SELECT m.id, m.salary, md.hours, md.deliveries, md.paymentDeliveries, md.bonus, md.retentionISR, md.pantryVouchers, md.extraRetentionISR, e.noEmployee, e.name, e.rolId, r.description
	FROM movements AS m
	INNER JOIN movement_detail AS md ON m.id = md.movementId
	INNER JOIN employees AS e ON m.employeeId = e.noEmployee
	INNER JOIN roles AS r on e.rolId = r.id
	WHERE m.month = _month AND m.year = _year AND m.status = 1
	ORDER BY m.id ASC;
END;
$BODY$;

select * From getMovementsByDate ('4', 2023);

CREATE OR REPLACE PROCEDURE createUser(name VARCHAR(250), email VARCHAR(50), password VARCHAR(100), rolId INTEGER)
LANGUAGE 'plpgsql'
AS $BODY$
BEGIN
    INSERT INTO users(name, email, password, rolId)
    VALUES (name, email, password, rolId); 
END
$BODY$;

CALL createUser(1, 'Juan Carlos Guerrero Ortiz', 2);
select * from users;