'use strict'

require('module-alias')(__dirname)
const db = require('@db/database/db')

async function init() {
  return await db()
}

init()