'use strict'
const database = require('../database/db')
const Se = require('sequelize')

exports.getByEmail = async function (email) {
  const { User } = await database()
  const response = await User.sequelize.query(`SELECT * From getUserByEmail('${email}')`, { type: Se.QueryTypes.SELECT })
  return response[0]
}

exports.create = async function (datos) {
  const { name, email, password, rolId, } = datos
  const { User } = await database()
  await User.sequelize.query(`CALL createUser('${name}', '${email}', '${password}', '${rolId}')`)
  return true
}
