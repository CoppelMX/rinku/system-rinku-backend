'use strict'
const database = require('../database/db')
const Se = require('sequelize')

exports.get = async function (month, year) {
  const { Movement } = await database()
  return await Movement.sequelize.query(`SELECT * From getMovementsByDate('${month}', '${year}')`, { type: Se.QueryTypes.SELECT })
}

exports.create = async function (data) {
  const { employeeId, month, year, salary, hours, deliveries, paymentDeliveries, bonus, retentionISR, pantryVouchers, extraRetentionISR } = data
  const { Movement } = await database()
  await Movement.sequelize.query(`CALL createMovement('${employeeId}', '${month}', '${year}', '${salary}', '${hours}', '${deliveries}', '${paymentDeliveries}', '${bonus}', '${retentionISR}', '${pantryVouchers}', '${extraRetentionISR}')`)
  return true
}
