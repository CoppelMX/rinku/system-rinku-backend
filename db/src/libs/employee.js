'use strict'

const database = require('../database/db')
const Se = require('sequelize')

exports.get = async function (search) {
  const { Employee } = await database()
  if (search) return await Employee.sequelize.query(`SELECT * From getEmployeeWithFilters('${search}')`, { type: Se.QueryTypes.SELECT })
  else return await Employee.sequelize.query(`SELECT * From getEmployees()`, { type: Se.QueryTypes.SELECT })
}

exports.getById = async function (id) {
  const { Employee } = await database()
  const response = await Employee.sequelize.query(`SELECT * From getByEmployeeById('${id}')`, { type: Se.QueryTypes.SELECT })
  return response[0]
}

exports.getByNoEmployee = async function (noEmployee) {
  const { Employee } = await database()
  const response = await Employee.sequelize.query(`SELECT * From getByNoEmployee('${noEmployee}')`, { type: Se.QueryTypes.SELECT })
  return response[0]
}

exports.create = async function (datos) {
  const { noEmployee, name, rolId } = datos
  const { Employee } = await database()
  await Employee.sequelize.query(`CALL createEmployee('${noEmployee}', '${name}', '${rolId}')`)
  return true
}

exports.update = async function (id, datos) {
  const { noEmployee, name, rolId } = datos
  const { Employee } = await database()
  await Employee.sequelize.query(`CALL updateEmployeeById('${id}', '${noEmployee}', '${name}', '${rolId}')`)
  return true
}