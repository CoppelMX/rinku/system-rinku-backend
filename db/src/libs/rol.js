'use strict'
const Se = require('sequelize')
const database = require('../database/db')

exports.get = async function () {
  const { Rol } = await database()
  return await Rol.sequelize.query(`SELECT * From getRoles()`, { type: Se.QueryTypes.SELECT })
}