'use strict'

const Sequelize = require('sequelize')

module.exports = (sequelize) => {

  return sequelize.define(
    'movements',
    {
      employeeId: {
        type: Sequelize.INTEGER,
        allowNull: false,
      },
      month: {
        type: Sequelize.CHAR,
        allowNull: false,
      },
      year: {
        type: Sequelize.STRING(4),
        allowNull: false,
      },
      salary: {
        type: Sequelize.DECIMAL(13, 4),
        allowNull: false,
      },
      status: {
        type: Sequelize.INTEGER,
        allowNull: false,
        defaultValue: 1,
      },
    },
    {
      freezeTableName: true,
    }
  )
}
