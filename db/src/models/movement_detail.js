'use strict'

const Sequelize = require('sequelize')

module.exports = (sequelize) => {

  return sequelize.define(
    'movement_detail',
    {
      movementId: {
        type: Sequelize.INTEGER,
        allowNull: false,
      },
      hours: {
        type: Sequelize.INTEGER,
        allowNull: false,
      },
      deliveries: {
        type: Sequelize.INTEGER,
        allowNull: false,
      },
      paymentDeliveries: {
        type: Sequelize.DECIMAL(13, 4),
        allowNull: false,
      },
      bonus: {
        type: Sequelize.DECIMAL(13, 4),
        allowNull: false,
      },
      retentionISR: {
        type: Sequelize.DECIMAL(13, 4),
        allowNull: false,
      },
      pantryVouchers: {
        type: Sequelize.DECIMAL(13, 4),
        allowNull: false,
      },
      extraRetentionISR: {
        type: Sequelize.DECIMAL(13, 4),
        allowNull: false,
      },
    },
    {
      freezeTableName: true,
    }
  )
}
