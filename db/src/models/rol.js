'use strict'

const Sequelize = require('sequelize')

module.exports = (sequelize) => {

  return sequelize.define(
    'roles',
    {
      description: {
        type: Sequelize.STRING(100),
        allowNull: false,
      },
      status: {
        type: Sequelize.INTEGER,
        allowNull: false,
        defaultValue: 1,
      },
    },
    {
      freezeTableName: true,
    }
  )
}
