'use strict'

const Sequelize = require('sequelize')

module.exports = (sequelize) => {

  return sequelize.define(
    'employees',
    {
      noEmployee: {
        type: Sequelize.INTEGER,
        allowNull: false,
      },
      name: {
        type: Sequelize.STRING(255),
        allowNull: false,
      },
      rolId: {
        type: Sequelize.INTEGER,
        allowNull: false,
      },
      status: {
        type: Sequelize.INTEGER,
        allowNull: false,
        defaultValue: 1,
      },
    },
    {
      freezeTableName: true,
    }
  )
}
