'use strict'

module.exports = function (sequelize) {
  const UserModel = require('../models/user')(sequelize)
  const EmployeeModel = require('../models/employee')(sequelize)
  const RolModel = require('../models/rol')(sequelize)
  const MovementModel = require('../models/movement')(sequelize)
  const MovementDetailModel = require('../models/movement_detail')(sequelize)

  RolModel.hasOne(EmployeeModel, { as: 'rol', key: 'rolId' })
  EmployeeModel.belongsTo(RolModel, { as: 'rol', key: 'rolId' })

  UserModel.belongsTo(RolModel, { as: 'rol', key: 'rolId' })

  MovementModel.hasOne(MovementDetailModel, { as: 'detalle', foreignKey: 'movementId' })
  MovementDetailModel.belongsTo(MovementModel, { as: 'detalle', foreignKey: 'movementId' })
  MovementModel.belongsTo(EmployeeModel, { as: 'employee', foreignKey: 'employeeId' })

  sequelize.User = UserModel,
  sequelize.Employee = EmployeeModel,
  sequelize.Rol = RolModel,
  sequelize.Movement = MovementModel,
  sequelize.MovementDetail = MovementDetailModel
}
