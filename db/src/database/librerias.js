'use strict'

const User = require('../libs/user')
const Employee = require('../libs/employee')
const Rol = require('../libs/rol')
const Movement = require('../libs/movement')

module.exports = {
  User,
  Employee,
  Rol,
  Movement
}
