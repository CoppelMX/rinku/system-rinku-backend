'use strict'

const Sequelize = require('sequelize')
const setModels = require('../database/models')
const config = require('./config')
const debug = require('debug')('system-rinku-back:db:conexion')

let sequelize = null

module.exports = async function () {
  try {
    if (!sequelize) {
      sequelize = new Sequelize(config.db)
      setModels(sequelize, Sequelize)
    }

    await sequelize.authenticate().then(() => {
      debug('Connection has been established successfully.')
    })
  } catch (error) {
    debug('Sequelize error: ', error)
    throw new Error('Unable to connect to the database.')
  }
  return sequelize
}