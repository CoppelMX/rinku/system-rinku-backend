'use strict'

const { NAMESPACE_DG, POSTGRESQL_HOST, POSTGRESQL_DATABASE, POSTGRESQL_USER, POSTGRESQL_PASSWORD, POSTGRESQL_PORT } = process.env

const debug = require('debug')(`${NAMESPACE_DG}:db:query`)

module.exports = {
  db: {
    host: POSTGRESQL_HOST,
    username: POSTGRESQL_USER,
    password: POSTGRESQL_PASSWORD,
    database: POSTGRESQL_DATABASE,
    port: POSTGRESQL_PORT,
    dialect: 'postgres',
    logging: (s) => debug(s),
    dialectOptions: {
      application_name: 'db',
    },
  },
}