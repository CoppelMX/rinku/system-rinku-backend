'use strict'

require('dotenv-flow').config()
require('module-alias/register')

const cors = require('cors')
const express = require('express')
const routes = require('@routes')
const bodyParser = require('body-parser')
const { PORT, NAMESPACE_DG } = process.env
const debug = require('debug')(`${NAMESPACE_DG}server`)
debug.enabled = true
const app = express()

app.use(cors())
app.use(express.json())
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: false }))
app.use('/api', routes)

app.listen(PORT, () => {
  debug(`El servidor esta corriendo en el puerto ${PORT}`)
})